package root.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

@Path("/")

public class SumaRecurso {
    
@GET
@Path("/suma")
public String getSumaQuery(@QueryParam("numeros") String numeros){
    String listaNumeros[] = numeros.split(",");
    Integer suma = 0;
    for(int i=0; i < listaNumeros.length; i++){
        suma = suma + Integer.parseInt(listaNumeros[i]);
    }
    return "La suma de los numeros " + numeros + " es: " + suma.toString();
}
    
}
